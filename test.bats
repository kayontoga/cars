#!/usr/bin/bats

@test "id.py without path" {
    run python3 id.py
    [ "$status" -eq 2 ]
    [ "${lines[0]}" = "usage: id.py [--view] path" ]
    [ "${lines[1]}" = "id.py: error: the following arguments are required: path" ]
}

@test "id.py with 404 url" {
    run python3 id.py https://togaware.com/fred.jpg
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "The URL does not appear to exist. Please check." ]
    [ "${lines[1]}" = "    https://togaware.com/fred.jpg" ]
}

@test "id.py test url image" {
    run python3 id.py --view https://www.supercars.net/blog/wp-content/uploads/2016/02/390646.jpg
    [ "$status" -eq 0 ]
    [ "$output" = "Jaguar,McLaren 12C,https://www.supercars.net/blog/wp-content/uploads/2016/02/390646.jpg" ]
}

@test "id.py test url image subaru" {
    run python3 id.py --view https://imgsvrcars-a.akamaihd.net/images/2017/9/SubaruXV/x.jpg/Subaru-XV-1.jpg
    [ "$status" -eq 0 ]
    [ "$output" = "Chevy,Vezel,https://imgsvrcars-a.akamaihd.net/images/2017/9/SubaruXV/x.jpg/Subaru-XV-1.jpg" ]
}

@test "id.py test url image tiggo" {
    run python3 id.py https://www.carprices.ae/wp-content/uploads/2018/02/Tiggo-5-e1501834485959.jpg
    [ "$status" -eq 0 ]
    [ "$output" = "Chrey,Tiggo 5,https://www.carprices.ae/wp-content/uploads/2018/02/Tiggo-5-e1501834485959.jpg" ]
}

@test "id.py test url image passat" {
    run python3 id.py https://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Passat-R36-Variant-4177_14.jpg
    [ "$status" -eq 0 ]
    [ "$output" = "Volkswagen,Volkswagen R36,https://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Passat-R36-Variant-4177_14.jpg" ]
}

@test "id.py test url image honda" {
    run python3 id.py http://s3.caradvice.com.au/wp-content/uploads/2017/05/2017-honda-civic-hatch-29.jpg
    [ "$status" -eq 0 ]
    [ "$output" = "Honda,New Focus hatchback,http://s3.caradvice.com.au/wp-content/uploads/2017/05/2017-honda-civic-hatch-29.jpg" ]
}

@test "id.py test url image bmw" {
    run python3 id.py http://st.automobilemag.com/uploads/sites/10/2015/09/2014-BMW-6-series-gran-coupe-three-quarters-view-2.jpg
    [ "$status" -eq 0 ]
    [ "$output" = "BWM,BWM 6 Series,http://st.automobilemag.com/uploads/sites/10/2015/09/2014-BMW-6-series-gran-coupe-three-quarters-view-2.jpg" ]
}

@test "id.py test url image merc" {
    run python3 id.py http://neo-drive.com/modules/images/files/images1280_/2011_mercedes-benz_e-class_sedan_e63-amg_fq_oem_2_500.jpg
    [ "$status" -eq 0 ]
    [ "$output" = "Benz,Benz C Class AMG,http://neo-drive.com/modules/images/files/images1280_/2011_mercedes-benz_e-class_sedan_e63-amg_fq_oem_2_500.jpg" ]
}

@test "id.py test url mlhub" {
    run curl --output /dev/null --silent --head --fail "https://mlhub.ai"
    [ "$status" -eq 0 ]
}

@test "id.py test url mlhub computer vision" {
    run curl --output /dev/null --silent --head --fail "https://onepager.togaware.com/Computer_Vision.html"
    [ "$status" -eq 0 ]
}

@test "id.py test url mmlab dataset" {
    run curl --output /dev/null --silent --head --fail "http://mmlab.ie.cuhk.edu.hk/datasets/comp_cars/index.html"
    [ "$status" -eq 0 ]
}

@test "id.py test url github jiajun" {
    run curl --output /dev/null --silent --head --fail "https://github.com/Schwartz-Zha/CompCarDemo"
    [ "$status" -eq 0 ]
}

@test "id.py test url gitlab cars" {
    run curl --output /dev/null --silent --head --fail "https://gitlab.com/kayontoga/cars"
    [ "$status" -eq 0 ]
}

@test "id.py test url pre-built model" {
    run curl --output /dev/null --silent --head --fail "https://drive.google.com/file/d/1c7sWyEGhO_YtuRUuZBDEkcs-WPsZV-7e/view?usp=sharing"
    [ "$status" -eq 0 ]
}

@test "id.py test url model and make names" {
    run curl --output /dev/null --silent --head --fail "https://github.com/Schwartz-Zha/CompCarDemo/blob/master/Dataset/data/misc/make_model_name.mat"
    [ "$status" -eq 0 ]
}

@test "id.py test url training data instructions" {
    run curl --output /dev/null --silent --head --fail "http://mmlab.ie.cuhk.edu.hk/datasets/comp_cars/instruction.txt"
    [ "$status" -eq 0 ]
}

