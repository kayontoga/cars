#!/usr/bin/env python3
#
# -*- coding: utf-8 -*-
#
# A script to identify the make and model of a car in an image.
#
# Source github:
# https://github.com/Schwartz-Zha/CompCarDemo
#
# Prebuilt model from
# https://drive.google.com/file/d/1lzWuGR1YT-F-RLVTLRqUUxXp0f3aJ5s_/view?usp=sharing
#
# ml id cars <path>
#
# ml id cars mycar.jpg
# ml id cars https:.../mycar.jpg
#
# Author Graham.Williams@anu.edu.au based on above repository by
# Jiajun Zha.

import io
import os
import sys
import argparse
import requests

import torch
import torchvision

import matplotlib.pyplot as plt
import scipy.io as skio

from torchvision import transforms
from PIL import Image

from mlhub.pkg import is_url, get_cmd_cwd

# ----------------------------------------------------------------------
# Parse command line arguments.
# ----------------------------------------------------------------------

option_parser = argparse.ArgumentParser(add_help=False)

option_parser.add_argument(
    'path',
    help='path or url to image')

option_parser.add_argument(
    '--view',
    action='store_true',
    help='view the image loaded for identification by the model')

args = option_parser.parse_args()

# ----------------------------------------------------------------------
# Load the image.
# ----------------------------------------------------------------------

path = args.path

# Check the supplied path or url exists and is an image.

if is_url(path):
    response = requests.get(path)
    if response.status_code != 200:
        print(f"The URL does not appear to exist. Please check.\n    {path}")
        sys.exit()
    img = Image.open(io.BytesIO(response.content))
else:
    path = os.path.join(get_cmd_cwd(), path)
    img = Image.open(path)

# Image size confined to H=224, W=224 for optimal use.

transform = transforms.Compose([
    transforms.Resize(( 224,224)),
    transforms.ToTensor()])

# GPU or CPU

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

#  Ensure 4 channel image.
 
img_tensor = transform(img).unsqueeze_(0).to(device)

# Optionally view the image.

if args.view:
    plt.imshow(img)
    plt.show()

# ----------------------------------------------------------------------
# SETUP
# ----------------------------------------------------------------------

# Load the make and model maps.

mapping_model = skio.loadmat('make_model_name.mat')['model_names'][:, 0]
mapping_make = skio.loadmat('make_model_name.mat')['make_names'][:, 0]

# ----------------------------------------------------------------------
# CLASSIFICATION MODELS
# ----------------------------------------------------------------------

checkpoint = torch.load('cp6_10_epoch.tar', map_location=device)

make = torchvision.models.resnet34(weights=None, progress=True)
make.fc = torch.nn.Linear(in_features = 512, out_features = 163)
make.load_state_dict(checkpoint['make_model_state_dict'])

model = torchvision.models.resnet50(weights=None, progress=True)
model.fc = torch.nn.Linear(in_features = 2048, out_features = 2004)
model.load_state_dict(checkpoint['model_model_state_dict'])

# Set the classifiers into evaluation mode.

make.eval() 
model.eval()

# Predict.

make_output = make(img_tensor)
model_output = model(img_tensor)

# Report.

_, predicted_make = make_output.max(1)
_, predicted_model = model_output.max(1)

print(f"{str(mapping_make[predicted_make.item()-1][0])}," +
      f"{str(mapping_model[predicted_model.item()-1][0])}," +
      f"{path}")
